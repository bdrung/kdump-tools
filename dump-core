#!/bin/bash
set -eu

# Copyright (C) 2017-2020, IONOS SE
# Authors: Benjamin Drung <benjamin.drung@cloud.ionos.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# This tool can be used to store core dumps the same way as kernel dumps.
# Set /proc/sys/kernel/core_pattern to pipe the core dumps to this tool.
# You can use following sysctl configuration file:
#
#   $ cat /etc/sysctl.d/50-dump-core.conf
#   kernel.core_pattern = |/usr/share/kdump-tools/dump-core %p %s %t %e
#   # 2 = "suidsafe": Any binary which normally would not be dumped (see "0")
#   # is dumped readable by root only.
#   fs.suid_dumpable = 2
#   $ sysctl -p /etc/sysctl.d/50-dump-core.conf

exec > >(logger -t "${0}" -p syslog.info) 2> >(logger -t "${0}" -p syslog.warn)

if test "$#" -ne 4; then
	echo "called with incorrect number of arguments: '$*'." >&2
	echo "Expected 4 arguments: pid, signal, timestamp, executable (core_pattern arguments: %p %s %t %e)." >&2
	exit 1
fi

PID="$1"
SIGNAL="$2"
TIMESTAMP="$3"
PROGRAM="$4"
echo "called for pid $PID, signal $SIGNAL, timestamp $TIMESTAMP, executable $PROGRAM"

# Global Setup
KDUMP_DEFAULTS=/etc/default/kdump-tools
[ -r $KDUMP_DEFAULTS ] && . $KDUMP_DEFAULTS

# Set up defaults
KDUMP_COREDIR=${KDUMP_COREDIR:=/var/crash}
NFS_TIMEO=${NFS_TIMEO:=600}
NFS_RETRANS=${NFS_RETRANS:=3}
KDUMP_STAMP="${PROGRAM// /-}-sig${SIGNAL}-$(date -u -d "@$TIMESTAMP" +%Y%m%d%H%M%S)"


#
# Return the name of the subdirectory to store core file.
#	Will add hostname/IP according to the value of
#	HOSTTAG if networked dump is selected

define_stampdir()
{
	STAMP=$1
	COREDIR="$2"
	HOSTTAG="${HOSTTAG:=ip}"

	if [ -z "${SSH-}" ] && [ -z "${NFS-}" ] && [ -z "${FTP-}" ]; then
		echo "$COREDIR/$STAMP"
	elif [ "$HOSTTAG" = "hostname" ];then
		echo "$COREDIR/$(hostname)-$STAMP"
	else
		# Looping to give time to network to settle
		local counter=0
		while [ $counter -lt 5 ];do
			THIS_HOST="$(ip addr show up | sed -n 's/^\s*inet\s\+\([^/ ]*\).*$/\1/p' | tail -n1)"
			# shellcheck disable=SC2086
			set -- $THIS_HOST
			THIS_HOST=$1
			if [ -z "$THIS_HOST" ]; then
				sleep 1
				counter=$((counter + 1))
			else
				break
			fi
		done
		if [ -z "$THIS_HOST" ]; then
			# Send log msg to stderr to avoid polluting
			# the result of the function
			echo "Unable to get IP from network" >&2
			echo "Reverting to HOSTTAG=hostname" >&2
			THIS_HOST="$(hostname)"
		fi
		echo "$COREDIR/$THIS_HOST-$STAMP"
	fi
}


check_compression() {
	case "$KDUMP_COMPRESSION" in
	  bzip2)
		if ! command -v bzip2 > /dev/null; then
			echo "Error: Compression set to bzip2, but bzip2 command not found. Disabling compression." >&2
			KDUMP_COMPRESSION=
		fi
		;;
	  gzip)
		if ! command -v gzip > /dev/null; then
			echo "Error: Compression set to gzip, but gzip command not found. Disabling compression." >&2
			KDUMP_COMPRESSION=
		fi
		;;
	  lz4)
		if ! command -v lz4 > /dev/null; then
			echo "Error: Compression set to lz4, but lz4 command not found. Disabling compression." >&2
			KDUMP_COMPRESSION=
		fi
		;;
	  xz)
		if test ! -x /usr/bin/xz; then
			echo "Error: Compression set to xz, but /usr/bin/xz not found. Disabling compression." >&2
			KDUMP_COMPRESSION=
		fi
		;;
	  *)
		if test -n "$KDUMP_COMPRESSION"; then
			echo "Error: Compression '$KDUMP_COMPRESSION' not supported. Disabling compression." >&2
			KDUMP_COMPRESSION=
		fi
	esac
}


compress() {
	case "$KDUMP_COMPRESSION" in
	  bzip2)
		bzip2 -c
		;;
	  gzip)
		gzip -c
		;;
	  lz4)
		lz4 -c
		;;
	  xz)
		/usr/bin/xz -c
		;;
	  *)
		cat
	esac
}

compression_extension() {
	case "$KDUMP_COMPRESSION" in
	  bzip2)
		echo ".bz2"
		;;
	  gzip)
		echo ".gz"
		;;
	  lz4)
		echo ".lz4"
		;;
	  xz)
		echo ".xz"
		;;
	esac
}


collect_metadata() {
	# Collect metadata information using apport's crash file format.
	# See http://bazaar.launchpad.net/~apport-hackers/apport/trunk/view/head:/doc/data-format.tex
	local coredump_file dependencies executable_path package package_version source_package
	coredump_file="$1"

	. /etc/os-release
	executable_path=$(readlink "/proc/$PID/exe")
	cat <<EOF
ProblemType: Crash
Architecture: $(dpkg --print-architecture)
CoreDumpFile: ${coredump_file##*/}
Date: $(date -d "@$TIMESTAMP" -R)
DistroRelease: ${NAME} ${VERSION_ID}
ExecutablePath: ${executable_path}
Host: $(uname -n)
EOF

	package=$(dpkg -S "$executable_path" 2>/dev/null | sed -s 's/:.*$//')
	if test -n "$package"; then
		package_version=$(dpkg -s "$package" | sed -n 's/Version: //p')
		if dpkg -s "$package" | grep -q '^Source:'; then
			source_package=$(dpkg -s "$package" | sed -n 's/Source: //p')
		else
			source_package=$package
		fi
		dependencies=$(apt-cache --no-suggests --no-conflicts --no-breaks --no-replaces --no-enhances --recurse --installed depends "${package}=${package_version}" | sed -n 's/.*: //p' | sort | uniq)
		echo "Package: ${package} ${package_version}"
	fi

	cat <<EOF
ProcCmdline: $(sed 's/ /\\ /g' "/proc/$PID/cmdline" | tr '\0' ' ' | sed 's/ *$//')
Signal: ${SIGNAL}
EOF
	if test -n "${source_package-}"; then
		echo "SourcePackage: ${source_package}"
	fi
	# shellcheck disable=SC2086
	cat <<EOF
Uname: $(uname -srm)
Dependencies:
$(dpkg-query -f=' ${Package} ${Version}\n' -W ${dependencies-})
ProcStatus:
$(sed 's/^/ /' "/proc/$PID/status")
ProcMaps:
$(sed 's/^/ /' "/proc/$PID/maps")
EOF
}


dump_local()
{
	KDUMP_STAMPDIR=$(define_stampdir "$KDUMP_STAMP" "$KDUMP_COREDIR")
	KDUMP_CORETEMP="$KDUMP_STAMPDIR/coredump-incomplete$(compression_extension)"
	KDUMP_COREFILE="$KDUMP_STAMPDIR/coredump.$KDUMP_STAMP$(compression_extension)"
	CRASH_FILE="$KDUMP_STAMPDIR/${KDUMP_STAMP}.crash"

	# If we use NFS, verify that we can mount the FS
	#
	if [ -n "${NFS-}" ];then
		echo "Mounting NFS mountpoint $NFS ..."
		if ! mount -t nfs -o nolock -o tcp -o soft -o timeo=${NFS_TIMEO} -o retrans=${NFS_RETRANS} "$NFS" "$KDUMP_COREDIR"; then
			echo "Unable to mount remote NFS directory $NFS. Cannot save core dump" >&2
			return 1;
		fi

		# FS is mounted, see if we can write to it
		#
		if ! mkdir -p "$KDUMP_STAMPDIR"; then
			echo "Unable to write to the remote NFS directory $NFS. Cannot save core dump" >&2
			if ! umount $KDUMP_COREDIR; then
				echo "Unable to cleanly unmount the NFS file system" >&2
			fi
		else
			echo "Dumping to NFS mountpoint $NFS/$KDUMP_STAMP"
		fi
	else
		mkdir -p "$KDUMP_STAMPDIR"
	fi

	collect_metadata "$KDUMP_COREFILE" > "$CRASH_FILE"

	if compress > "$KDUMP_CORETEMP"; then
		mv "$KDUMP_CORETEMP" "$KDUMP_COREFILE"
		echo "saved core dump in $KDUMP_STAMPDIR"
		sync
	else
		ERROR=$?
		echo "failed to save core dump in $KDUMP_STAMPDIR" >&2
	fi

	# If we use NFS, umount the remote FS
	#
	if [ -n "$NFS" ];then
		if ! umount "$KDUMP_COREDIR"; then
			echo "Unable to cleanly unmount the NFS file system" >&2
		fi
	fi

	return ${ERROR-0}
}


dump_to_ftp()
{
	local fqdn
	FTP_REMOTE_HOST="${FTP%%:*}"
	fqdn=$(host "${FTP_REMOTE_HOST}" | grep -v "not found" | tail -n 1 | cut -d " " -f 1 || echo "${FTP_REMOTE_HOST}")
	FTP_COREDIR="${FTP#*:}"
	if [ "$FTP_COREDIR" = "$FTP" ]; then
		# No colon in FTP specified. Use / as path
		FTP_COREDIR="/"
	fi

	FTP_STAMPDIR=$(define_stampdir "" "${FTP_COREDIR}")

	FTP_COREFILE="${FTP_STAMPDIR}$KDUMP_STAMP$(compression_extension)"
	CRASH_FILE="${FTP_STAMPDIR}${KDUMP_STAMP}.crash"

	FTPPUT_ARGS=""
	if [ -n "${FTP_USER-}" ]; then
		FTPPUT_ARGS="$FTPPUT_ARGS -u $FTP_USER"
	fi
	if [ -n "${FTP_PASSWORD-}" ]; then
		FTPPUT_ARGS="$FTPPUT_ARGS -p $FTP_PASSWORD"
	fi
	if [ -n "${FTP_PORT-}" ]; then
		FTPPUT_ARGS="$FTPPUT_ARGS -P $FTP_PORT"
	fi

	# save meta-data
	# shellcheck disable=SC2086
	if collect_metadata "$FTP_COREFILE" | busybox ftpput $FTPPUT_ARGS "$FTP_REMOTE_HOST" "$CRASH_FILE" -; then
		echo "saved crash meta-data via FTP in $fqdn:$CRASH_FILE"
	else
		ERROR=$?
		echo "failed to save crash meta-data via FTP in $fqdn:$CRASH_FILE" >&2
	fi

	# shellcheck disable=SC2086
	if compress | busybox ftpput $FTPPUT_ARGS "$FTP_REMOTE_HOST" "$FTP_COREFILE" -; then
		echo "saved core dump via FTP in $fqdn:$FTP_COREFILE"
	else
		ERROR=$?
		echo "failed to save core dump via FTP in $fqdn:$FTP_COREFILE" >&2
	fi

	return ${ERROR-0}
}


dump_to_ssh()
{
	SSH_KEY="${SSH_KEY:=/root/.ssh/kdump_id_rsa}"
	SSH_REMOTE_HOST="$SSH"

	SSH_STAMPDIR=$(define_stampdir "$KDUMP_STAMP" "$KDUMP_COREDIR")

	SSH_CORETEMP="$SSH_STAMPDIR/coredump-incomplete$(compression_extension)"
	SSH_COREFILE="$SSH_STAMPDIR/coredump.$KDUMP_STAMP$(compression_extension)"
	CRASH_FILE="${SSH_STAMPDIR}/${KDUMP_STAMP}.crash"

	if ! ssh -i "$SSH_KEY" "$SSH_REMOTE_HOST" mkdir -p "$SSH_STAMPDIR"; then
		# If remote connections fails, no need to continue
		echo "Unable to reach remote server $SSH_REMOTE_HOST. No reason to continue" >&2
		return 1
	fi

	# save meta-data
	if collect_metadata "$SSH_COREFILE" | ssh -i "$SSH_KEY" "$SSH_REMOTE_HOST" dd "of=$CRASH_FILE"; then
		echo "saved crash meta-data via SSH in $SSH_REMOTE_HOST:$CRASH_FILE"
	else
		ERROR=$?
		echo "failed to save crash meta-data via SSH in $SSH_REMOTE_HOST:$CRASH_FILE" >&2
	fi

	echo "sending core dump to $SSH_REMOTE_HOST : $SSH_CORETEMP"
	if compress | ssh -i "$SSH_KEY" "$SSH_REMOTE_HOST" dd "of=$SSH_CORETEMP"; then
		ssh -i "$SSH_KEY" "$SSH_REMOTE_HOST" mv "$SSH_CORETEMP" "$SSH_COREFILE"
		echo "saved core dump in $SSH_REMOTE_HOST:$SSH_STAMPDIR"
	else
		ERROR=$?
		echo "failed to save core dump in $SSH_REMOTE_HOST:$SSH_STAMPDIR" >&2
	fi

	return ${ERROR-0}
}

if test -n "${KDUMP_TASKSET_CORES-}"; then
	taskset -p -c "${KDUMP_TASKSET_CORES}" $$ || true
fi

check_compression

if [ -n "${FTP-}" ]; then
	dump_to_ftp
fi
if [ -n "${SSH-}" ]; then
	dump_to_ssh
fi
if [ -n "${NFS-}" ] || [ -z "${FTP-}${SSH-}" ]; then
	dump_local
fi
